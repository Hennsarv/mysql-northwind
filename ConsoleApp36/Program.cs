﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace ConsoleApp36
{
    class Program
    {
        static void Main(string[] args)
        {
            string password = Console.ReadLine();
            string database = "northwind";
            using (MySqlConnection conn = new MySqlConnection($"Server=hennu-mysql.mysql.database.azure.com; Port=3306; Database={database}; Uid=Henn@hennu-mysql; Pwd={password}; SslMode=Preferred;"))
            {
                conn.Open();
                using (MySqlCommand comm = new MySqlCommand("select * from Customers;", conn))
                {
                    var R = comm.ExecuteReader();
                    while(R.Read())
                    {
                        Console.WriteLine($"{R[0]}\t{R[1]}");
                    }
                }
                conn.Close();
            }
        }
    }
}
